#!/bin/bash

sudo apt update
sudo apt install -y dirmngr gnupg apt-transport-https
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com --recv 7568D9BB55FF9E5287D586017AE645C0CF8E292A
wget -qO - https://www.mongodb.org/static/pgp/server-5.0.asc | sudo apt-key add -
sudo tee /etc/apt/sources.list.d/pritunl.list << EOF
deb https://repo.pritunl.com/stable/apt bullseye main
EOF
echo "deb http://repo.mongodb.org/apt/debian bullseye/mongodb-org/5.0 main" | sudo tee /etc/apt/sources.list.d/mongodb-org-5.0.list
sudo apt update
sudo apt install -y wireguard-tools pritunl mongodb-org
sudo systemctl enable mongod pritunl
sudo systemctl start mongod pritunl
echo "Connect to HTTPS and type the setup key :"
sudo pritunl setup-key
echo "IP is :"
curl ipinfo.io/ip

